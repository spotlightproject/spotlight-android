package org.uniglobalunion.spotlight

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import androidx.core.content.ContextCompat
import org.json.JSONException
import org.json.JSONObject
import org.uniglobalunion.spotlight.model.StudyListing
import org.uniglobalunion.spotlight.security.PreferencesSettings
import org.uniglobalunion.spotlight.service.AlarmIntentService
import org.uniglobalunion.spotlight.service.TrackingService
import java.io.IOException
import java.util.*

class SpotlightApp : Application() {

    val mCleanInsights = CleanInsightsManager()

    override fun onCreate() {
        super.onCreate()
        initializeData()
        if (PreferencesSettings.getPrefBool(this, "serviceRunning")) {
            updateAlarms()
        }
        mCleanInsights.initMeasurement(this)

    }

    fun getCleanInsights (): CleanInsightsManager {
        return mCleanInsights
    }

    fun updateAlarms() {
        setWakeupAlarm()
        setInsightAlarm()
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    fun startStudy(studyId: String?) {
        val intent = Intent(this, TrackingService::class.java)
        intent.action = TrackingService.ACTION_START_TRACKING
        intent.putExtra(TrackingService.EXTRA_STUDY_ID, studyId)
        ContextCompat.startForegroundService(this, intent)
    }

    fun startEnabledStudies() {
        val intent = Intent(this, TrackingService::class.java)
        intent.action = TrackingService.ACTION_START_ENABLED_STUDIES
        ContextCompat.startForegroundService(this, intent)
    }

    fun stopStudy(studyId: String?) {
        val intent = Intent(this, TrackingService::class.java)
        intent.action = TrackingService.ACTION_STOP_TRACKING
        intent.putExtra(TrackingService.EXTRA_STUDY_ID, studyId)
        ContextCompat.startForegroundService(this, intent)
    }

    fun updateServicePrefs() {
        val intent = Intent(this, TrackingService::class.java)
        intent.action = TrackingService.ACTION_UPDATE_PREFS
        ContextCompat.startForegroundService(this, intent)
    }

    /**
     * Removes activity recognition updates using
     * failure callbacks.
     */
    fun stopService() {
        val intent = Intent(this, TrackingService::class.java)
        intent.action = TrackingService.ACTION_STOP_TRACKING
        startService(intent)
        stopService(intent)
    }

    /**
     * Retrieves the boolean from SharedPreferences that tracks whether we are requesting activity
     * updates.
     */
    private val updatesRequestedState: Boolean
        private get() = PreferenceManager.getDefaultSharedPreferences(this)
            .getBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED, false)

    fun getStudies(enabledOnly: Boolean): Collection<StudyListing> {
        initializeData()
        return if (enabledOnly) {
            val studyListings = ArrayList<StudyListing>()
            for (listing in mStudyMap!!.values) {
                if (listing.isEnabled) studyListings.add(listing)
            }
            studyListings
        } else mStudyMap!!.values
    }

    fun getStudy(studyId: String): StudyListing? {
        initializeData()
        return mStudyMap!![studyId]
    }

    private var mStudyMap: HashMap<String, StudyListing>? = null
    private var mLastLang: String? = null

    // This method creates an ArrayList that has three Person objects
    // Checkout the project associated with this tutorial on Github if
    // you want to use the same images.
    @Synchronized
    private fun initializeData() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val currLang = Locale.getDefault().language
        if (mLastLang == null || currLang != mLastLang) {
            mStudyMap = null
            mLastLang = currLang
        }
        if (mStudyMap == null) {
            mStudyMap = HashMap()
            try {
                var studyJsonPath = "studies/$mLastLang/studies.json"
                if (!doesAssetExist(studyJsonPath)) studyJsonPath = "studies/en/studies.json"
                val obj = JSONObject(loadStringFromAsset(studyJsonPath))
                val m_jArry = obj.getJSONArray("studies")
                for (i in 0 until m_jArry.length()) {
                    val jo_inside = m_jArry.getJSONObject(i)

                    /**
                     * "id": "study0",
                     * "name": "Are You Moving?",
                     * "description": "hey now",
                     * "icon": "/studies/study_sitting.png",
                     * "geo": false,
                     * "apps": false,
                     * "activities": true,
                     * "environment": true,
                     * "event": true
                     */
                    val studyId = jo_inside.getString("id")
                    val name = jo_inside.getString("name")
                    val desc = jo_inside.getString("description")
                    val iconPath = jo_inside.getString("icon")
                    val trackGeo = jo_inside.getBoolean("geo")
                    val trackApps = jo_inside.getBoolean("apps")
                    val trackActivities = jo_inside.getBoolean("activities")
                    val trackEnv = jo_inside.getBoolean("environment")
                    val trackEvt = jo_inside.getBoolean("event")
                    val trackCommute = jo_inside.getBoolean("commute")
                    val trackTime = jo_inside.getBoolean("time")
                    val cameraUpload = jo_inside.getBoolean("camera upload")
                    val voiceUpload = jo_inside.getBoolean("voice upload")
                    val studyListing = StudyListing(
                        studyId,
                        name,
                        desc,
                        iconPath,
                        trackGeo,
                        trackActivities,
                        trackApps,
                        trackEnv,
                        trackEvt,
                        trackCommute,
                        trackTime,
                        cameraUpload,
                        voiceUpload
                    )
                    if (jo_inside.has("order")) studyListing.order = jo_inside.getInt("order")
                    val isEnabled = prefs.getBoolean(
                        Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId,
                        false
                    )
                    studyListing.isEnabled = isEnabled
                    mStudyMap!![studyListing.id] = studyListing
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mCleanInsights.persistMeasurements()
    }

    override fun onTerminate() {
        super.onTerminate()
        mCleanInsights.persistMeasurements()
    }

    fun doesAssetExist(path: String?): Boolean {
        return try {
            val `is` = assets.open(path!!)
            val size = `is`.available()
            `is`.close()
            true
        } catch (ex: IOException) {
            false
        }
    }

    fun loadStringFromAsset(path: String?): String? {
        var json: String? = null
        json = try {
            val `is` = assets.open(path!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }


    private var alarmMgr: AlarmManager? = null
    private var alarmIntentStartOfDay: PendingIntent? = null
    private var alarmIntentInsights: PendingIntent? = null
    private val REQUEST_CODE_WAKE_UP = 9009
    private val REQUEST_CODE_INSIGHTS = 9010
    fun setWakeupAlarm() {
        alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
        if (alarmIntentStartOfDay != null) alarmMgr!!.cancel(alarmIntentStartOfDay)
        var startHour = 0
        var startMin = 1
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val prefStartTime = prefs.getString(Constants.PREF_START_TIME, "6:00")
        var splitChar = ":"
        if (prefStartTime!!.contains(".")) splitChar = "\\."
        val startParts = prefStartTime.split(splitChar).toTypedArray()
        if (startParts.size >= 2) {
            try {
                startHour = startParts[0].toInt() - 2 // start time minus 2 hours
                startMin = startParts[1].toInt()
            } catch (e: Exception) {
                Log.d(javaClass.name, "error parsing day start", e)
            }
        }


//        Intent intent = new Intent(this, AlarmReceiver.class);
        //alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        val intent = Intent(this, AlarmIntentService::class.java)
        var flags = PendingIntent.FLAG_UPDATE_CURRENT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        }

        alarmIntentStartOfDay = PendingIntent.getService(
            this,
            REQUEST_CODE_WAKE_UP,
            intent,
            flags
        )

        // Set the alarm to start at approximately the start of the work day
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = startHour
        calendar[Calendar.MINUTE] = startMin

        //just once a day!
        alarmMgr!!.setRepeating(
            AlarmManager.RTC_WAKEUP, calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY, alarmIntentStartOfDay
        )
    }

    fun setInsightAlarm() {
        if (alarmMgr == null) alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
        if (alarmIntentInsights != null) alarmMgr!!.cancel(alarmIntentInsights)
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val prefStartTime = prefs.getString(Constants.PREF_INSIGHT_TIME, "8:00")
        var splitChar = ":"
        if (prefStartTime!!.contains(".")) splitChar = "\\."
        var startHour = 8
        var startMin = 1
        val startParts = prefStartTime.split(splitChar).toTypedArray()
        if (startParts.size >= 2) {
            try {
                startHour = startParts[0].toInt() // start time minus 2 hours
                startMin = startParts[1].toInt()
            } catch (e: Exception) {
                Log.d(javaClass.name, "error parsing insight time", e)
            }
        }
        val intent = Intent(this, AlarmIntentService::class.java)
        intent.putExtra(TrackingService.KEY_INSIGHTS, true)
        var flags = PendingIntent.FLAG_UPDATE_CURRENT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        }
        alarmIntentInsights = PendingIntent.getService(
            this,
            REQUEST_CODE_INSIGHTS,
            intent,
            flags
        )

        // Set the alarm to start at approximately the start of the work day
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = startHour
        calendar[Calendar.MINUTE] = startMin

        //just once a day!
        alarmMgr!!.setRepeating(
            AlarmManager.RTC_WAKEUP, calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY, alarmIntentInsights
        )
    }


}