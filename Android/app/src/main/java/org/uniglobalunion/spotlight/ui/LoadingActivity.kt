package org.uniglobalunion.spotlight.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.uniglobalunion.spotlight.BuildConfig
import org.uniglobalunion.spotlight.R
import org.uniglobalunion.spotlight.security.LockScreenActivity
import org.uniglobalunion.spotlight.security.PreferencesSettings
import org.uniglobalunion.spotlight.service.TrackingService

class LoadingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        if (PreferencesSettings.getPrefBool(this, "locked")) {
            finish()
            lockApp()
        } else {
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            if (!prefs.getBoolean("onboarding", false)) {
                startActivity(Intent(this, OnboardingActivity::class.java))
            } else {
                try {
                    startActivity(Intent(this, Class.forName(BuildConfig.MAIN_ACTIVITY)))
                    if (PreferencesSettings.getPrefBool(this, "serviceRunning")) {
                        val iService = Intent(this, TrackingService::class.java)
                        iService.setAction(TrackingService.ACTION_START_TRACKING)
                        startService(iService)
                    }
                    if (intent.hasExtra("isNotify") && intent.getBooleanExtra("isNotify", false)) {
                        val intentNotify = Intent(this, NotifyInfoActivity::class.java)
                        intentNotify.putExtra("title", intent.getStringExtra("title"))
                        intentNotify.putExtra("message", intent.getStringExtra("message"))
                        startActivity(intentNotify)
                    }
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }
            }
            finish()
        }
    }

    private fun lockApp() {
        finish()
        val intent = Intent(this, LockScreenActivity::class.java)
        startActivity(intent)
    }


}
