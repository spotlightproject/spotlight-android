package org.uniglobalunion.spotlight.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;

@Entity(tableName = "study_table")
@TypeConverters({Converters.class})
public class Study {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "study_id")
    private String mStudyId;

    @NonNull
    @ColumnInfo(name = "study_name")
    private String mStudyName;

    @NonNull
    @ColumnInfo(name = "study_inputs")
    private ArrayList<String> mStudyInputs;

    public Study(@NonNull String studyId, @NonNull String studyName) {
        this.mStudyId = studyId;
        this.mStudyName = studyName;
    }

    public String getStudyId(){return this.mStudyId;}

    public int getId () { return this.id; }

    public void setId(int id) {
        this.id = id;
    }

    public void setStudyInputs(ArrayList<String> studyInputs) {
        this.mStudyInputs = studyInputs;
    }

    public ArrayList<String> getStudyInputs ()
    {
        return mStudyInputs;
    }

    public String getStudyName () { return this.mStudyName; }

    public void setStudyName(String studyName) {
        this.mStudyName = studyName;
    }

}
