package org.uniglobalunion.spotlight.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {StudyEvent.class, Study.class}, version = 4, exportSchema = false)
public abstract class StudyRoomDatabase extends RoomDatabase {

    public abstract StudyDao studyEventDao();

    private static volatile StudyRoomDatabase INSTANCE;

    public static StudyRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (StudyRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            StudyRoomDatabase.class, "study_database")
                            .addMigrations(MIGRATION_2_3,MIGRATION_3_4)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE study_table (id INTEGER NOT NULL, "
                    + "study_id TEXT NOT NULL, PRIMARY KEY(id))");

        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            database.execSQL("ALTER TABLE study_table "
                  + " ADD COLUMN study_name STRING");

            database.execSQL("ALTER TABLE study_table "
                    + " ADD COLUMN study_inputs STRING");

        }
    };
}
