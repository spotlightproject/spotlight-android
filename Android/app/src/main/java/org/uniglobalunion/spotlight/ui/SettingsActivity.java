package org.uniglobalunion.spotlight.ui;

import static org.uniglobalunion.spotlight.Constants.PREF_AUTO_UPLOAD;
import static org.uniglobalunion.spotlight.Constants.PREF_END_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_HIGHRES_LOCATION;
import static org.uniglobalunion.spotlight.Constants.PREF_INSIGHT_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_START_TIME;
import static org.uniglobalunion.spotlight.Constants.PROGRAM_PARTICIPANT_STATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.service.UploadWorker;

import java.util.concurrent.TimeUnit;

public class SettingsActivity extends AppCompatActivity {

    private static WorkManager mWorkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        mWorkManager = WorkManager.getInstance(getApplication());
    }

    @Override
    protected void onPause() {
        super.onPause();

        ((SpotlightApp)getApplication()).setWakeupAlarm();
        ((SpotlightApp)getApplication()).updateServicePrefs();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

            Preference pref;
            pref = findPreference(PREF_HIGHRES_LOCATION);
            pref.setOnPreferenceChangeListener((preference, newValue) -> {

                CheckBoxPreference prefCb = (CheckBoxPreference)preference;

                prefs.edit().putBoolean(PREF_HIGHRES_LOCATION,prefCb.isChecked()).commit();

                return true;
            });

            pref = findPreference(PREF_AUTO_UPLOAD);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            if (sharedPrefs.contains(PROGRAM_PARTICIPANT_STATE) &&
                    sharedPrefs.getString(PROGRAM_PARTICIPANT_STATE, "").equals("HCW_Wage_Theft")) {
                pref.setVisible(true);
                pref.setOnPreferenceChangeListener((preference, newValue) -> {

                    CheckBoxPreference oldPref = (CheckBoxPreference) preference;
                    Boolean newPref = (Boolean) newValue;

                    if (oldPref.isChecked() != newPref) {
                        prefs.edit().putBoolean(PREF_AUTO_UPLOAD, newPref).apply();
                        mWorkManager.cancelAllWorkByTag(PREF_AUTO_UPLOAD);

                        if (newPref) {
                            // Schedule daily automatic uploads
                            PeriodicWorkRequest workRequest =
                                    new PeriodicWorkRequest.Builder(UploadWorker.class, 1, TimeUnit.DAYS)
                                            .addTag(PREF_AUTO_UPLOAD)
                                            .build();
                            mWorkManager.enqueueUniquePeriodicWork(PREF_AUTO_UPLOAD, ExistingPeriodicWorkPolicy.REPLACE, workRequest);
                        } else {
                            // Send a final upload before autoupload is deleted
                            OneTimeWorkRequest workRequest =
                                    new OneTimeWorkRequest.Builder(UploadWorker.class).addTag(PREF_AUTO_UPLOAD).build();
                            mWorkManager.enqueue(workRequest);
                        }
                    }
                    return true;
                });
            } else {
                pref.setVisible(false);
            }

            pref = findPreference("pref_disable_battery");
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    disableBatteryOptimization();
                    return true;
                }
            });

            pref = findPreference("pref_setup_wiz");
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(getActivity(), OnboardingActivity.class));
                    return true;
                }
            });

            pref = findPreference(PREF_START_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_START_TIME,"8:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "8:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

                        if (hourOfDay == 0 && minute == 0)
                            minute = 1;

                        prefs.edit().putString(PREF_START_TIME,hourOfDay + ":" + minute).commit();

                    }
                });
                return true;
            });

            pref = findPreference(PREF_END_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_END_TIME,"18:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "18:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, (view, hourOfDay, minute, second) -> {

                    if (hourOfDay == 0 && minute == 0) {
                        hourOfDay = 11;
                        minute = 59;
                    }

                    prefs.edit().putString(PREF_END_TIME,hourOfDay + ":" + minute).commit();

                });
                return true;
            });

            pref = findPreference(PREF_INSIGHT_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_INSIGHT_TIME,"8:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "8:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, (view, hourOfDay, minute, second) -> {

                    if (hourOfDay == 0 && minute == 0) {
                        hourOfDay = 11;
                        minute = 59;
                    }

                    prefs.edit().putString(PREF_INSIGHT_TIME,hourOfDay + ":" + minute).commit();

                    ((SpotlightApp)getActivity().getApplication()).setInsightAlarm();

                });
                return true;
            });


        }

        private void disableBatteryOptimization ()
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Intent intent = new Intent();
                String packageName = getActivity().getPackageName();
                PowerManager pm = (PowerManager) getActivity().getSystemService(POWER_SERVICE);
                if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                    intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    intent.setData(Uri.parse("package:" + packageName));
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getActivity(),getActivity().getString(R.string.status_power_opt_disabled),Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public static void showTimePicker(FragmentActivity act, int hours, int minutes, int seconds, TimePickerDialog.OnTimeSetListener listener) {

        //if metric, use 24 hour mode
        boolean use24HourClock = DateFormat.is24HourFormat(act.getApplicationContext());
        TimePickerDialog mTimePickerDialog = TimePickerDialog.newInstance(listener, hours, minutes, seconds, use24HourClock);

        mTimePickerDialog.enableSeconds(false);

        mTimePickerDialog.show(act.getSupportFragmentManager(), "TimeDelayPickerDialog");

    }


}