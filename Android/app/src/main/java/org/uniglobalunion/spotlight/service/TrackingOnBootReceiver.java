package org.uniglobalunion.spotlight.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import org.uniglobalunion.spotlight.security.PreferencesSettings;

public class TrackingOnBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (PreferencesSettings.getPrefBool(context,"serviceRunning")) {
            Intent i = new Intent(context, TrackingService.class);
            ContextCompat.startForegroundService(context, i);
        }
    }
}