package org.uniglobalunion.spotlight.ui;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;
import static org.uniglobalunion.spotlight.Constants.PREF_DAYS_OF_WEEK;
import static org.uniglobalunion.spotlight.Constants.PREF_GUID;
import static org.uniglobalunion.spotlight.Constants.PREF_LOCATION_DISCLOSURE;
import static org.uniglobalunion.spotlight.Constants.PROGRAM_PARTICIPANT_STATE;
import static org.uniglobalunion.spotlight.service.TrackingService.logEvent;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.APPS_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_HOME_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_WORK_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.STEPS_REQUEST_CODE;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.PickVisualMediaRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.quickbirdstudios.surveykit.AnswerFormat;
import com.quickbirdstudios.surveykit.FinishReason;
import com.quickbirdstudios.surveykit.NavigableOrderedTask;
import com.quickbirdstudios.surveykit.NavigationRule;
import com.quickbirdstudios.surveykit.StepIdentifier;
import com.quickbirdstudios.surveykit.SurveyTheme;
import com.quickbirdstudios.surveykit.TaskIdentifier;
import com.quickbirdstudios.surveykit.TextChoice;
import com.quickbirdstudios.surveykit.result.StepResult;
import com.quickbirdstudios.surveykit.result.TaskResult;
import com.quickbirdstudios.surveykit.result.question_results.BooleanQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.DateQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.IntegerQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.MultipleChoiceQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.SingleChoiceQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.TextQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.TimeQuestionResult;
import com.quickbirdstudios.surveykit.steps.CompletionStep;
import com.quickbirdstudios.surveykit.steps.InstructionStep;
import com.quickbirdstudios.surveykit.steps.QuestionStep;
import com.quickbirdstudios.surveykit.steps.Step;
import com.quickbirdstudios.surveykit.survey.SurveyView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.sensors.PermissionManager;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;

public class SurveyActivity extends AppCompatActivity {

    SurveyView mSurveyView;

    SharedPreferences mPrefs;

    String survey = "onboarding";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
        mSurveyView = findViewById(R.id.survey_view);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (getIntent()!=null&&getIntent().hasExtra("survey")&&getIntent().getStringExtra("survey").equals("work")) {
            initWorkJournalSurvey();
        }
        else if (getIntent()!=null&&getIntent().hasExtra("survey")&&getIntent().getStringExtra("survey").equals("camera")) {
            // Registers a photo picker activity launcher in single-select mode.
            ActivityResultLauncher<PickVisualMediaRequest> pickMedia =
                    registerForActivityResult(new ActivityResultContracts.PickVisualMedia(), uri -> {
                        // Callback is invoked after the user selects a media item or closes the
                        // photo picker.
                        if (uri != null) {
                            Log.d("PhotoPicker", "Selected URI: " + uri);

                            int flag = Intent.FLAG_GRANT_READ_URI_PERMISSION;
                            getApplicationContext().getContentResolver().takePersistableUriPermission(uri, flag);

                            initCameraUploadSurvey(uri);
                        } else {
                            Log.e("PhotoPicker", "No media selected");

                            finish();
                        }
                    });

            pickMedia.launch(new PickVisualMediaRequest.Builder()
                    .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly.INSTANCE)
                    .build());
        }
        else if (getIntent()!=null && getIntent().hasExtra("survey") && getIntent().getStringExtra("survey").equals("data_date")) {
            initDataDateSurvey();
        }
        else
            initOnboardingSurvey ();
    }

    private void initDataDateSurvey() {
        survey = "data_date";
        ArrayList<Step> steps = new ArrayList<>();
        steps.add(new QuestionStep(
                "What is the first day of data you want uploaded?", "",
                getString(R.string.action_next),
                new AnswerFormat.DateAnswerFormat(),
                false, new StepIdentifier("step_start_date")));

        steps.add(new QuestionStep(
                "What is the last day of data you want uploaded?", "",
                getString(R.string.action_next),
                new AnswerFormat.DateAnswerFormat(),
                false, new StepIdentifier("step_end_date")));

        List<String> prefDays = Arrays.asList(mPrefs.getString(PREF_DAYS_OF_WEEK, "").split(","));

        ArrayList<TextChoice> listChoices = new ArrayList<TextChoice>();
        listChoices.add(new TextChoice(getString(R.string.day_sunday),"0"));
        listChoices.add(new TextChoice(getString(R.string.day_monday),"1"));
        listChoices.add(new TextChoice(getString(R.string.day_tuesday),"2"));
        listChoices.add(new TextChoice(getString(R.string.day_wed),"3"));
        listChoices.add(new TextChoice(getString(R.string.day_thursday),"4"));
        listChoices.add(new TextChoice(getString(R.string.day_frday),"5"));
        listChoices.add(new TextChoice(getString(R.string.day_saturday),"6"));

        ArrayList<TextChoice> defaultChoices = new ArrayList();
        for (TextChoice listChoice : listChoices) {
            if (prefDays.contains(listChoice.getValue())) {
                defaultChoices.add(listChoice);
            }
        }

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_days_of_week),"",getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(listChoices,defaultChoices), true, new StepIdentifier("step_days")));

        NavigableOrderedTask task = new NavigableOrderedTask(steps,new TaskIdentifier("task1"));

        mSurveyView.setOnSurveyFinish(new Function2() {
            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult) o;
                FinishReason reason = (FinishReason) o2;

                if (reason == FinishReason.Completed) {

                    Intent data = new Intent();
                    StringBuilder logText = new StringBuilder();

                    for (StepResult stepResult : result.getResults()) {
                        String stepId = stepResult.getId().getId();

                        switch (stepId) {
                            case "step_start_date":
                                String startDate = getDateAnswerToString(((DateQuestionResult) stepResult.getResults().get(0)).getAnswer());
                                data.putExtra("start_date", startDate);
                                logText.append("start_date:").append(startDate).append(",");
                                break;
                            case "step_end_date":
                                String endDate = getDateAnswerToString(((DateQuestionResult) stepResult.getResults().get(0)).getAnswer());
                                data.putExtra("end_date", endDate);
                                logText.append("end_date:").append(endDate).append(",");
                                break;
                            case "step_days":
                                // We currently don't do anything with this
                                logText.append("days_to_download:").append((stepResult.getResults().get(0)).getStringIdentifier()).append(",");
                                break;
                        }
                        logEvent(SurveyActivity.this, StudyEvent.EVENT_TYPE_REPORT,logText.toString());
                    }
                    setResult(RESULT_OK, data);
                }
                else if (reason == FinishReason.Discarded)
                {

                }
                else if (reason == FinishReason.Failed)
                {

                }

                finish();

                return null;
            }

        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);

    }

    private String getDateAnswerToString(AnswerFormat.DateAnswerFormat.Date dateAnswer) {
        // There seems to be an error where the month returns one index lower
        if (dateAnswer != null)
            return String.format(Locale.ENGLISH, "%d:%d:%d",
                    dateAnswer.getMonth()+1, dateAnswer.getDay(), dateAnswer.getYear());
        else
            return "";
    }

    private void initCameraUploadSurvey(Uri imageUri){
        survey = "camera";

        ArrayList<Step> steps = new ArrayList<>();

        steps.add(new QuestionStep(getString(R.string.image_description),"",getString(R.string.action_next),new AnswerFormat.TextAnswerFormat(100, "image description", new Function1<String, Boolean>() {
            @Override
            public Boolean invoke(String s) {
                return true;
            }
        }),true,new StepIdentifier("image_description")));

        steps.add(new CompletionStep(getString(R.string.camera_upload_confirmation),"",getString(R.string.action_okay),null, 1, false,new StepIdentifier("step_final")));

        NavigableOrderedTask task = new NavigableOrderedTask(steps,new TaskIdentifier("task1"));

        mSurveyView.setOnSurveyFinish(new Function2() {

            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult) o;
                FinishReason reason = (FinishReason) o2;

                if (reason == FinishReason.Completed) {

                    String description = "";

                    for (StepResult stepResult : result.getResults()) {
                        String stepId = stepResult.getId().getId();

                        if (stepId.equals("image_description"))
                        {
                            description = ((TextQuestionResult)stepResult.getResults().get(0)).getAnswer();
                        }

                    }

                    logEvent(SurveyActivity.this, StudyEvent.EVENT_TYPE_CAMERA,"Pic:" + imageUri.toString() + ",Description:" + description);

                }
                else if (reason == FinishReason.Discarded)
                {

                }
                else if (reason == FinishReason.Failed)
                {

                }

                finish();

                return null;
            }

        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);
    }


    private void initWorkJournalSurvey () {
        survey = "work";

        ArrayList<Step> steps = new ArrayList<>();

        // Step 0
        ArrayList<TextChoice> listOverworkedChoices = new ArrayList<>();
        listOverworkedChoices.add(new TextChoice(new String(Character.toChars(0x1F62B)) + getString(R.string.overworked_exhausted), "exhausted"));
        listOverworkedChoices.add(new TextChoice(new String(Character.toChars(0x1F61F)) + getString(R.string.overworked_kinda_exhausted), "kinda exhausted"));
        listOverworkedChoices.add(new TextChoice(new String(Character.toChars(0x1F610)) + getString(R.string.overworked_neutral), "neutral"));
        listOverworkedChoices.add(new TextChoice(new String(Character.toChars(0x1F60A)) + getString(R.string.overworked_kinda_energized), "kinda energized"));
        listOverworkedChoices.add(new TextChoice(new String(Character.toChars(0x1F601)) + getString(R.string.overworked_energized), "energized"));
        QuestionStep overall_mood = new QuestionStep(
                getString(R.string.work_journal_overworked_emotional), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listOverworkedChoices, new ArrayList<TextChoice>()),
                false, new StepIdentifier("overworked_emotional"));
        steps.add(overall_mood);

        // Step 1
        QuestionStep shift_review = new QuestionStep(
                getString(R.string.work_journal_like_to_say_more),
                getString(R.string.work_journal_like_to_say_more_text),
                getString(R.string.action_next),
                new AnswerFormat.TextAnswerFormat(100, getString(R.string.positive),
                        new Function1<String, Boolean>() {
                            @Override
                            public Boolean invoke(String s) {
                                return true;
                            }
                        }),
                true,new StepIdentifier("shift_review"));
        steps.add(shift_review);

        // Step 2
        ArrayList<TextChoice> listExtraTimeChoices = new ArrayList<>();
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x23F0)) + "\t" + getString(R.string.extra_time_client_overtime), "client_overtime"));
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x1F4DE)) + "\t" + getString(R.string.extra_time_contacted), "contact"));
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x1F3C3)) + "\t" + getString(R.string.extra_time_errands), "errands"));
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x1F454)) + "\t" + getString(R.string.extra_time_agency), "agency"));
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x1F4B8)) + "\t" + getString(R.string.extra_time_time_problems), "time_problems"));
        listExtraTimeChoices.add(new TextChoice(new String(Character.toChars(0x1F4D6)) + "\t" + getString(R.string.extra_time_work_training), "work_training"));
        listExtraTimeChoices.add(new TextChoice(getString(R.string.survey_other), "other"));
        listExtraTimeChoices.add(new TextChoice(getString(R.string.none), "none"));
        QuestionStep extra_time = new QuestionStep(
                getString(R.string.extra_time_question), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listExtraTimeChoices, new ArrayList<TextChoice>()),
                false, new StepIdentifier("extra_time"));
        steps.add(extra_time);

        // Step 3
        ArrayList<TextChoice> listExtraMoneyChoices = new ArrayList<>();
        listExtraMoneyChoices.add(new TextChoice(new String(Character.toChars(0x1F68C)) + "\t" + getString(R.string.extra_money_travel), "travel"));
        listExtraMoneyChoices.add(new TextChoice(new String(Character.toChars(0x1F637)) + "\t" + getString(R.string.extra_money_supplies), "supplies"));
        listExtraMoneyChoices.add(new TextChoice(new String(Character.toChars(0x1F381)) + "\t" + getString(R.string.extra_money_gift), "gift"));
        listExtraMoneyChoices.add(new TextChoice(getString(R.string.survey_other), "other"));
        listExtraMoneyChoices.add(new TextChoice(getString(R.string.none), "none"));
        QuestionStep extra_money = new QuestionStep(
                getString(R.string.extra_money_question), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listExtraMoneyChoices, new ArrayList<TextChoice>()),
                false, new StepIdentifier("extra_money"));
        steps.add(extra_money);

        // Step 4
        ArrayList<TextChoice> listOutsideCarePlan = new ArrayList<>();
        listOutsideCarePlan.add(new TextChoice(new String(Character.toChars(0x1FA7A)) + "\t" + getString(R.string.outside_care_plan_health), "health"));
        listOutsideCarePlan.add(new TextChoice(new String(Character.toChars(0x1F4CB)) + "\t" + getString(R.string.outside_care_plan_nonhealth), "nonhealth"));
        listOutsideCarePlan.add(new TextChoice(new String(Character.toChars(0x1F46A)) + "\t" + getString(R.string.outside_care_plan_family), "family"));
        listOutsideCarePlan.add(new TextChoice(new String(Character.toChars(0x1F469)) + "\t" + getString(R.string.outside_care_plan_other_care_workers), "careworkers"));
        listOutsideCarePlan.add(new TextChoice(getString(R.string.survey_other), "other"));
        listOutsideCarePlan.add(new TextChoice(getString(R.string.none), "none"));
        QuestionStep outside_care_plan = new QuestionStep(
                getString(R.string.outside_care_plan), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listOutsideCarePlan, new ArrayList<TextChoice>()),
                false, new StepIdentifier("outside_care_plan"));
        steps.add(outside_care_plan);

        // Step 5
        ArrayList<TextChoice> listReasons = new ArrayList<>();
        listReasons.add(new TextChoice(new String(Character.toChars(0x1F4C8)) + "\t" + getString(R.string.reason_performance), "performance"));
        listReasons.add(new TextChoice(new String(Character.toChars(0x1F496)) + "\t" + getString(R.string.reason_good_relats), "good_relats"));
        listReasons.add(new TextChoice(new String(Character.toChars(0x1F645)) + "\t" + getString(R.string.reason_bad_relats), "bad_relats"));
        listReasons.add(new TextChoice(new String(Character.toChars(0x1F4CB)) + "\t" + getString(R.string.reason_mistake), "mistake"));
        listReasons.add(new TextChoice(new String(Character.toChars( 0x1F4F1)) + "\t" + getString(R.string.reason_tech), "tech"));
        listReasons.add(new TextChoice(new String(Character.toChars(0x1F4F5)) + "\t" + getString(R.string.reason_agency), "agency"));
        listReasons.add(new TextChoice(getString(R.string.survey_other), "other"));
        listReasons.add(new TextChoice(getString(R.string.none), "none"));
        QuestionStep reasons_for_extra_work = new QuestionStep(
                getString(R.string.reason_question), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listReasons, new ArrayList<TextChoice>()),
                false, new StepIdentifier("reasons_for_extra_work"));
        steps.add(reasons_for_extra_work);

        // Step 6
        ArrayList<TextChoice> listImpacts = new ArrayList<>();
        listImpacts.add(new TextChoice(new String(Character.toChars(0x1F44D)) + "\t" + getString(R.string.impacts_satisfaction), "satisfaction"));
        listImpacts.add(new TextChoice(new String(Character.toChars(0x1F4AF)) + "\t" + getString(R.string.impacts_care), "care"));
        listImpacts.add(new TextChoice(new String(Character.toChars(0x2705)) + "\t" + getString(R.string.impacts_control), "control"));
        listImpacts.add(new TextChoice(new String(Character.toChars(0x1F634)) + "\t" + getString(R.string.impacts_emotional), "emotional"));
        listImpacts.add(new TextChoice(new String(Character.toChars(0x2696)) + "\t" + getString(R.string.impacts_balance), "balance"));
        listImpacts.add(new TextChoice(getString(R.string.survey_other), "other"));
        listImpacts.add(new TextChoice(getString(R.string.none), "none"));
        QuestionStep impacts_extra_work = new QuestionStep(
                getString(R.string.impacts_question), "",
                getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(
                        listImpacts, new ArrayList<TextChoice>()),
                false, new StepIdentifier("impacts_extra_work"));
        steps.add(impacts_extra_work);

        // Step 7
        QuestionStep survey_followup = new QuestionStep(
                getString(R.string.extra_work_survey_followup), "",
                getString(R.string.action_next),
                new AnswerFormat.TextAnswerFormat(
                        100, "",
                        new Function1<String, Boolean>() {
                            @Override
                            public Boolean invoke(String s) {
                                return true;
                            }
                        }
                ),
                true, new StepIdentifier("survey_followup"));
        steps.add(survey_followup);

        // Step 8
        CompletionStep work_journal_complete = new CompletionStep(getString(R.string.work_journal_complete),"",getString(R.string.action_okay),null, 1, false, new StepIdentifier("work_journal_complete"));
        steps.add(work_journal_complete);

        NavigableOrderedTask task = new NavigableOrderedTask(steps, new TaskIdentifier("work_journal_task"));

        mSurveyView.setOnSurveyFinish(new Function2()
        {

            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult)o;
                FinishReason reason = (FinishReason)o2;

                if (reason == FinishReason.Completed)
                {
                    ArrayList<String> answers = new ArrayList<>();

                    for (StepResult stepResult : result.getResults())
                    {
                        String stepId = stepResult.getId().getId();
                        String answer = "";

                        // Text questions
                        if (stepId.equals("shift_review")
                                || stepId.contains("survey_followup")) {
                            answer = ((TextQuestionResult) stepResult.getResults().get(0)).getAnswer();
                        }

                        // Multiple choice questions
                        else if (stepId.equals("extra_time")
                                || stepId.equals("extra_money")
                                || stepId.equals("overworked_emotional")
                                || stepId.equals("outside_care_plan")
                                || stepId.equals("reasons_for_extra_work")
                                || stepId.equals("impacts_extra_work")) {
                            answer = ((MultipleChoiceQuestionResult) stepResult.getResults().get(0)).getStringIdentifier();
                        }

                        if (!answer.equals("")) {
                            answers.add(stepId + ":" + answer);
                        }
                    }

                    logEvent(SurveyActivity.this,
                            StudyEvent.EVENT_TYPE_REPORT,
                            String.join(";", answers));
                }
                else if (reason == FinishReason.Discarded)
                {

                }
                else if (reason == FinishReason.Failed)
                {

                }

                finish();

                return null;
            }


        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);
    }

    private void initOnboardingSurvey ()
    {

        ArrayList<Step> steps = new ArrayList<>();

        int STEP_TRIAL = 0;
        ArrayList<TextChoice> surveyChoices = new ArrayList<TextChoice>();
        surveyChoices.add(new TextChoice(getString(R.string.non_program_participant),""));
        surveyChoices.add(new TextChoice(getString(R.string.program_participant_of_HCW_wage_theft),"HCW_Wage_Theft"));
        steps.add(new QuestionStep(getString(R.string.part_of_a_program),"",getString(R.string.action_next),
                new AnswerFormat.SingleChoiceAnswerFormat(surveyChoices,surveyChoices.get(0)),false, new StepIdentifier("step_research_program_participation")));

        int STEP_CODE = 1;
        steps.add(new QuestionStep(getString(R.string.part_of_a_program_code),getString(R.string.part_of_a_program_followup),getString(R.string.action_next),
                new AnswerFormat.IntegerAnswerFormat(null, "0987"), true, new StepIdentifier("step_program_code")));

        int STEP_WORKDAYS = 2;
        ArrayList<TextChoice> listChoices = new ArrayList<TextChoice>();
        listChoices.add(new TextChoice(getString(R.string.day_sunday),"0"));
        listChoices.add(new TextChoice(getString(R.string.day_monday),"1"));
        listChoices.add(new TextChoice(getString(R.string.day_tuesday),"2"));
        listChoices.add(new TextChoice(getString(R.string.day_wed),"3"));
        listChoices.add(new TextChoice(getString(R.string.day_thursday),"4"));
        listChoices.add(new TextChoice(getString(R.string.day_frday),"5"));
        listChoices.add(new TextChoice(getString(R.string.day_saturday),"6"));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_days_of_week),"",getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(listChoices,new ArrayList<TextChoice>()), false, new StepIdentifier("step_time_days")));

        int STEP_WORK_SCHEDULE = 3;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_work_schedule_yes),getString(R.string.survey_question_regular_work_schedule_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_fixed_time")));

        int STEP_WORKDAY_BEGINS = 4;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_when_begin),"",getString(R.string.action_next),
                new AnswerFormat.TimeAnswerFormat(new AnswerFormat.TimeAnswerFormat.Time(8,0)), false, new StepIdentifier("step_time_start")));

        int STEP_WORKDAY_ENDS = 5;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_when_end),"",getString(R.string.action_next),
                new AnswerFormat.TimeAnswerFormat(new AnswerFormat.TimeAnswerFormat.Time(18,0)), false, new StepIdentifier("step_time_stop")));

        int STEP_ENABLE_LOCATION = 6;
        steps.add(new QuestionStep(getString(R.string.location_permission_disclosure),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.location_permission_disclosure_ok),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_enable_location")));

        int STEP_SET_HOME_LOCATION = 7;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_home_location),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_home_location_action),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_set_home_location")));

        int STEP_WORK_LOCATION = 8;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_place),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_work_place_yes),getString(R.string.survey_question_regular_work_place_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_fixed_location")));

        int STEP_SET_WORK_LOCATION = 9;
        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_location),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_work_location_action),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_set_work_location")));

        int STEP_PHONE_WORK = 10;
        steps.add(new QuestionStep(getString(R.string.survey_question_work_apps),getString(R.string.survey_question_work_apps_desc),getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_work_apps_yes),getString(R.string.survey_question_work_apps_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_use_apps")));

        int STEP_ENABLE_USAGE_TRACKING = 11;
        steps.add(new QuestionStep(getString(R.string.survey_question_work_apps_permission_prompt),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_work_apps_allow_access),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_use_apps_permission")));

        int STEP_CHOOSE_APPS = 12;
        steps.add(new QuestionStep(getString(R.string.survey_question_work_apps_choose_apps),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_work_apps_choose_apps_prompt),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_use_apps_choose")));

        int STEP_ENABLE_ACTIVITY = 13;
        steps.add(new QuestionStep(getString(R.string.survey_question_enable_activity_title),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_enable_activity_prompt),getString(R.string.not_now),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_enable_activity")));


        int STEP_COMPLETE = 14;
        steps.add(new CompletionStep(getString(R.string.survey_configure_success),"",getString(R.string.action_next),null,1,false,new StepIdentifier("stepfinal")));

        NavigableOrderedTask task = new NavigableOrderedTask(steps,new TaskIdentifier("task1"));

        task.setNavigationRule(steps.get(STEP_TRIAL).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.non_program_participant)))
                    return steps.get(STEP_WORKDAYS).getId();
                else
                    return steps.get(STEP_CODE).getId();
            }
        }));

        task.setNavigationRule(steps.get(STEP_WORK_SCHEDULE).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes)))
                    return steps.get(STEP_WORKDAY_BEGINS).getId();
                else
                    return steps.get(STEP_ENABLE_LOCATION).getId();
            }
        }));

        task.setNavigationRule(steps.get(STEP_ENABLE_LOCATION).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.location_permission_disclosure_ok))) {
                    checkLocationPermissions();
                    return steps.get(STEP_SET_HOME_LOCATION).getId();
                }
                else {
                    return steps.get(STEP_PHONE_WORK).getId();
                }

            }
        }));

        task.setNavigationRule(steps.get(STEP_WORK_LOCATION).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes))) {
                    return steps.get(STEP_SET_WORK_LOCATION).getId();
                }
                else {
                    return steps.get(STEP_PHONE_WORK).getId();
                }
            }
        }));


        task.setNavigationRule(steps.get(STEP_SET_WORK_LOCATION).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {
                if (!s.startsWith(getString(R.string.not_now))) {
                    initLocationPicker(MAP_WORK_REQUEST_CODE);
                }
                return steps.get(STEP_PHONE_WORK).getId();
            }
        }));

        task.setNavigationRule(steps.get(STEP_SET_HOME_LOCATION).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (!s.startsWith(getString(R.string.not_now))) {
                    initLocationPicker(MAP_HOME_REQUEST_CODE);
                }
                return steps.get(STEP_WORK_LOCATION).getId();

            }
        }));

        task.setNavigationRule(steps.get(STEP_ENABLE_USAGE_TRACKING).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (!s.startsWith(getString(R.string.not_now))) {
                    enableUsageTrackingDialog();
                    return steps.get(STEP_CHOOSE_APPS).getId();
                } else {
                    return steps.get(STEP_ENABLE_ACTIVITY).getId();
                }

            }
        }));

        task.setNavigationRule(steps.get(STEP_CHOOSE_APPS).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {
                if (!s.startsWith(getString(R.string.not_now))) {
                    chooseApps();
                }
                return steps.get(STEP_ENABLE_ACTIVITY).getId();
            }
        }));

        task.setNavigationRule(steps.get(STEP_ENABLE_ACTIVITY).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {
                if (!s.startsWith(getString(R.string.not_now))) {
                    enableActivityTracking();
                }
                return steps.get(STEP_COMPLETE).getId();
            }
        }));


        mSurveyView.setOnSurveyFinish(new Function2()
        {

            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult)o;
                FinishReason reason = (FinishReason)o2;

                if (reason == FinishReason.Completed)
                {

                    for (StepResult stepResult : result.getResults())
                    {
                        String stepId = stepResult.getId().getId();

                        if (stepId.equals("step_research_program_participation")) {
                            SingleChoiceQuestionResult scqResult = (SingleChoiceQuestionResult) result.getResults().get(0).getResults().get(0);
                            String scqResultText = scqResult.getAnswer().getValue();

                            mPrefs.edit().putString(PROGRAM_PARTICIPANT_STATE, scqResultText).apply();

                            if (scqResultText.equals("HCW_Wage_Theft")) {

                                enableStudy("study1", true);    // movement
                                enableStudy("study2", true);    // journal
                                enableStudy("study3", true);    // work app usage
                                enableStudy("study4", true);    // locations and dist
                                enableStudy("study5", true);    // commute time
                                enableStudy("study6", false);    // time tracking
                                enableStudy("study7", true);    // camera upload
                                enableStudy("study8", false);    // voice upload
                            }
                        }
                        else if (stepId.equals("step_program_code")) {
                            Integer intResult = ((IntegerQuestionResult)stepResult.getResults().get(0)).getAnswer();
                            if (intResult != null && intResult != 0) {
                                mPrefs.edit().putString(PREF_GUID, intResult.toString()).apply();
                            } else {
                                mPrefs.edit().putString(PROGRAM_PARTICIPANT_STATE, "").apply();
                            }
                        }
                        else if (stepId.equals("step_enable_location")) {

                            boolean enabledLocation = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;
                            enableStudy("study4", enabledLocation);
                            mPrefs.edit().putBoolean(PREF_LOCATION_DISCLOSURE,enabledLocation).apply();

                        }
                        else if (stepId.equals("step_time_days")) {
                            MultipleChoiceQuestionResult mqresult = (MultipleChoiceQuestionResult)stepResult.getResults().get(0);

                            StringBuffer sb = new StringBuffer();
                            for (TextChoice tchoice: mqresult.getAnswer())
                            {
                                sb.append(tchoice.getValue()).append(",");
                            }

                            mPrefs.edit().putString(PREF_DAYS_OF_WEEK,sb.toString()).apply();
                        }
                        else if (stepId.equals("step_time_start") || stepId.equals("step_time_stop")) {
                            TimeQuestionResult timeResult = (TimeQuestionResult) stepResult.getResults().get(0);
                            mPrefs.edit().putString(stepId,timeResult.getStringIdentifier()).apply();
                        }
                        else if (mPrefs.getString(PROGRAM_PARTICIPANT_STATE, "").isEmpty()) {
                            switch (stepId) {
                                case "step_fixed_time": {
                                    boolean isFixedHours = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;

                                    if (isFixedHours) {

                                        //get start and end time and days of the week

                                        enableStudy("study1", true); //daily movement
                                        enableStudy("study2", true); //work log
                                        enableStudy("study3", true); //app usage
                                    }
                                    else
                                    {
                                        enableStudy("study1", true); //daily movement
                                        enableStudy("study2", true); //work log
                                        enableStudy("study3", false); //app usage
                                        enableStudy("study5", false); //commute time
                                    }
                                    break;
                                }
                                case "step_fixed_location": {
                                    boolean isFixedLocation = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;

                                    if (isFixedLocation)
                                    {
                                        enableStudy("study4",true); //location
                                        enableStudy("study5",true); //commute time
                                        enableStudy("study0",false); //environmental
                                        enableStudy("study2",true); //work log

                                    }
                                    else
                                    {
                                        enableStudy("study4",true); //location
                                        enableStudy("study5",false); //commute time
                                        enableStudy("study1",true); //daily movement
                                        enableStudy("study0",false); //environmental
                                        enableStudy("study2",true); //work log

                                    }
                                    break;
                                }
                                case "step_use_apps": {
                                    boolean usesApps = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;
                                    enableStudy("study3", usesApps); //app usage
                                    break;
                                }
                            }
                        }
                    }

                    if (!mPrefs.contains(PREF_GUID)) {
                        mPrefs.edit().putString(PREF_GUID, UUID.randomUUID().toString()).apply();
                    }

                    startActivity(new Intent(SurveyActivity.this,OnboardingSuccess.class));
                    finish();

                }
                else if (reason == FinishReason.Discarded)
                {
                    finish();
                }
                else if (reason == FinishReason.Failed)
                {
                    finish();
                }


                return null;
            }


        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);
    }

    private void enableStudy(String studyId,boolean enabled)
    {

        String key = Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId;

        if (!enabled)
        {
            mPrefs.edit().remove(key).commit();
        }
        else
        {
            mPrefs.edit()
                    .putBoolean(key, enabled).commit();
        }

        StudyListing sl = ((SpotlightApp)getApplication()).getStudy(studyId);
        if (sl != null) {
            sl.isEnabled = enabled;

            if (enabled)
                startStudy(studyId);
        }
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy(String studyId) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkLocationPermissions() {
        PermissionManager.checkLocationPermissions(this, 1233, false);
    }

    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(this);

        startActivityForResult(locationPickerIntent, requestCode);
    }

    private boolean checkForAppsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    private void enableUsageTrackingDialog () {

        if (!checkForAppsPermission(this)) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.prompt_app_usage_permission)).setPositiveButton(R.string.ok_pf, dialogClickListener)
                    .setNegativeButton(R.string.cancel_pf, dialogClickListener).show();

        }
        else
        {
            Toast.makeText(this,"Permission is enabled",Toast.LENGTH_SHORT).show();
        }

    }

    private void chooseApps ()
    {
        startActivityForResult(new Intent(this,AppManagerActivity.class),APPS_REQUEST_CODE);

    }

    private void enableActivityTracking() {
        PermissionManager.checkStepPermission(this, STEPS_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK || requestCode == APPS_REQUEST_CODE)
        {
            if (requestCode == MAP_HOME_REQUEST_CODE || requestCode == MAP_WORK_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                String geoKey = "home";
                if (requestCode == MAP_WORK_REQUEST_CODE)
                    geoKey = "work";

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs
                        .edit()
                        .putFloat("geo-" + geoKey + "-lat", (float) latitude)
                        .putFloat("geo-" + geoKey + "-long", (float) longitude)
                        .putString("geo-" + geoKey + "-address", address)
                        .putString("geo-" + geoKey + "-postalcode", postalcode)
                        .apply();


                startStudy("study5");

            }
        } else {
            finish();
        }
    }
}




