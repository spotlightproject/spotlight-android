package org.uniglobalunion.spotlight.sensors;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.uniglobalunion.spotlight.service.TrackingService;

public abstract class LocationMonitor {

    public abstract void startLocationUpdates(TrackingService context, boolean highResLoc);

    public abstract void startGeofencing (TrackingService context, SharedPreferences mPrefs);

    public abstract void stopGeofencing (TrackingService context);

    public abstract void stopLocationTracking ();


    public static boolean isGooglePlayServicesAvailable(Context context){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }

}
