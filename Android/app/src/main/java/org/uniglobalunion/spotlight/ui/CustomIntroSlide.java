package org.uniglobalunion.spotlight.ui;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.uniglobalunion.spotlight.R;

public class CustomIntroSlide extends Fragment {

    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private int layoutResId;
    private String title;
    private String body;

    public static CustomIntroSlide newInstance(int layoutResId) {
        CustomIntroSlide sampleSlide = new CustomIntroSlide();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    public static CustomIntroSlide newInstance(int layoutResId, String title, String body) {
        CustomIntroSlide sampleSlide = new CustomIntroSlide();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        args.putString("title",title);
        args.putString("body",body);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID)) {
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
        }

        title = getArguments().getString("title",null);
        body = getArguments().getString("body",null);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutResId, container, false);

        if (!TextUtils.isEmpty(title))
            ((TextView)view.findViewById(R.id.title)).setText(title);

        if (!TextUtils.isEmpty(body))
            ((TextView)view.findViewById(R.id.body)).setText(body);

        return view;

    }
}