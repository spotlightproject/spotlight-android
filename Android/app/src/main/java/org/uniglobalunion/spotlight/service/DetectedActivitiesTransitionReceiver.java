package org.uniglobalunion.spotlight.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyRepository;

import java.util.ArrayList;
import java.util.Date;

public class DetectedActivitiesTransitionReceiver extends BroadcastReceiver {


    public static final String INTENT_ACTION = "org.uniglobalunion.spotlight" +
            ".ACTION_PROCESS_ACTIVITY_TRANSITIONS";
    private final static String TAG = "DABR";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"got activity: " + intent);


        if (ActivityTransitionResult.hasResult(intent)) {
                ActivityTransitionResult result = ActivityTransitionResult.extractResult(intent);

                if (result != null) {
                    for (ActivityTransitionEvent event : result.getTransitionEvents())
                        processTransitionResultEvent(context, event);
                }
            }

            if (ActivityRecognitionResult.hasResult(intent)) {

//If data is available, then extract the ActivityRecognitionResult from the Intent//
                ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

//Get an array of DetectedActivity objects//
                ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();
                for (DetectedActivity event : detectedActivities)
                    processActivityResultEvent(context, event);

            }

    }


    public static void processActivityResultEvent(Context context,DetectedActivity event)
    {

        String studyId = "activity";
        String studyData = event.getConfidence() + "," + event.getType() + "," + getActivityString(context,event.getType());
        StudyEvent studyEvent = new StudyEvent(studyId,new Date().getTime(),studyData);
        TrackingService.getRepository(context).insert(studyEvent);

        Toast.makeText(context,"activity:" + studyData, Toast.LENGTH_SHORT).show();

    }

    public static void processTransitionResultEvent(Context context,ActivityTransitionEvent event)
    {

        String studyId = "transition";
        String studyData = event.getActivityType() + "," + event.getTransitionType() + "," + event.getElapsedRealTimeNanos();
        StudyEvent studyEvent = new StudyEvent(studyId,new Date().getTime(),studyData);
        TrackingService.getRepository(context).insert(studyEvent);

        Toast.makeText(context,"transition:" + studyData, Toast.LENGTH_SHORT).show();

    }

    static String getActivityString(Context context, int detectedActivityType) {
        Resources resources = context.getResources();
        switch(detectedActivityType) {
            case DetectedActivity.ON_BICYCLE:
                return resources.getString(R.string.on_bicycle);
            case DetectedActivity.ON_FOOT:
                return resources.getString(R.string.on_foot);
            case DetectedActivity.RUNNING:
                return resources.getString(R.string.running);
            case DetectedActivity.STILL:
                return resources.getString(R.string.still);
            case DetectedActivity.TILTING:
                return resources.getString(R.string.tilting);
            case DetectedActivity.WALKING:
                return resources.getString(R.string.walking);
            case DetectedActivity.IN_VEHICLE:
                return resources.getString(R.string.in_vehicle);
            default:
                return resources.getString(R.string.unidentifiable_activity, detectedActivityType);
        }
    }
    static final int[] POSSIBLE_ACTIVITIES = {

            DetectedActivity.STILL,
            DetectedActivity.ON_FOOT,
            DetectedActivity.WALKING,
            DetectedActivity.RUNNING,
            DetectedActivity.IN_VEHICLE,
            DetectedActivity.ON_BICYCLE,
            DetectedActivity.TILTING,
            DetectedActivity.UNKNOWN
    };

}
