package org.uniglobalunion.spotlight.ui;

import static org.uniglobalunion.spotlight.Constants.PREF_DAYS_OF_WEEK;
import static org.uniglobalunion.spotlight.Constants.PREF_GUID;
import static org.uniglobalunion.spotlight.Constants.PROGRAM_PARTICIPANT_STATE;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amplifyframework.AmplifyException;
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin;
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession;
import com.amplifyframework.core.Amplify;
import com.amplifyframework.logging.AndroidLoggingPlugin;
import com.amplifyframework.logging.LogLevel;
import com.amplifyframework.storage.options.StorageUploadFileOptions;
import com.amplifyframework.storage.s3.AWSS3StoragePlugin;
import com.google.android.material.snackbar.Snackbar;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.model.StudyViewModel;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.slybeaver.slycalendarview.SlyCalendarDialog;

public class StudyResultsActivity extends AppCompatActivity {

    private RecyclerView rv;

    private StudyListing mStudyListing;
    private ArrayList<String> mStudyIdFilter;

    Date dateStart, dateEnd;
    TextView tvTimeFilter;

    Handler mHandler = new Handler();
    static String REPORT_FILE_NAME;

    enum FileAction {
        NONE,
        SHARE,
        DOWNLOAD,
        SEND
    }

    FileAction mNextFileAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_results);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTimeFilter = findViewById(R.id.timefilter);


        initDates();

        String studyId = getIntent().getStringExtra("studyId");

        mStudyIdFilter = new ArrayList<>();

        if (!TextUtils.isEmpty(studyId)) {
            mStudyListing = ((SpotlightApp) getApplication()).getStudy(studyId);

            if (mStudyListing != null)
            {
                if (mStudyListing.cameraUpload) {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_CAMERA);
                }
                if (mStudyListing.trackActivities) {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_MOTION);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_STEPS);
                }

                if (mStudyListing.trackEnviron)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TEMPERATURE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_LIGHT);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_HUMIDITY);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SOUND);
                }


                if (mStudyListing.trackCommute)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_COMMUTE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_FENCE);
                }
                else if (mStudyListing.trackGeo)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_LOGGING);
                }

                if (mStudyListing.trackEvent)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_REPORT);
                }

                if (mStudyListing.trackApps)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SCREEN);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_APP_USAGE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_DEVICE_POWER);
                }

                if (mStudyListing.trackTime)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TIME);
                }

            }
        }

        rv = (RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        initializeData();

        REPORT_FILE_NAME = getReportFileName(this);

        mNextFileAction = FileAction.NONE;

        try {
            Amplify.addPlugin(new AndroidLoggingPlugin(LogLevel.VERBOSE));
            Amplify.addPlugin(new AWSCognitoAuthPlugin());
            Amplify.addPlugin(new AWSS3StoragePlugin());
            Amplify.configure(getApplicationContext());
            Log.i("Amplify", "Initialized Amplify");
        } catch (AmplifyException error) {
            Log.e("Amplify", "Could not initialize Amplify", error);
        }

    }

    public static String getReportFileName(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!sharedPrefs.contains(PREF_GUID)) {
            sharedPrefs.edit().putString(PREF_GUID, UUID.randomUUID().toString()).apply();
        }
        DateFormat df = new SimpleDateFormat("MMddyy-HHmm", Locale.US);
        return "report-" + sharedPrefs.getString(PREF_GUID, "") + "-" + df.format(new Date()) + ".csv";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_result_menu, menu);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        menu.findItem(R.id.menu_send).setVisible(sharedPrefs.contains(PROGRAM_PARTICIPANT_STATE) &&
                sharedPrefs.getString(PROGRAM_PARTICIPANT_STATE, "").equals("HCW_Wage_Theft"));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        else if (item.getItemId() == R.id.menu_download) {
            mNextFileAction = FileAction.DOWNLOAD;
            selectDatesForFile();
        }
        else if (item.getItemId() == R.id.menu_share) {
            mNextFileAction = FileAction.SHARE;
            selectDatesForFile();
        }
        else if (item.getItemId() == R.id.menu_send) {
            mNextFileAction = FileAction.SEND;
            selectDatesForFile();
        }
        else if (item.getItemId() == R.id.menu_delete) {
            deleteAllData();
        }
        else if (item.getItemId() == R.id.menu_delete_selected) {
            deleteSelectedData();
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeDates (View view)
    {
        showDatePicker();
    }

    private void showDatePicker ()
    {
        new SlyCalendarDialog()
                .setSingle(false)
                .setCallback(new SlyCalendarDialog.Callback() {
                    @Override
                    public void onCancelled() {

                    }

                    @Override
                    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {

                        firstDate.set(Calendar.HOUR_OF_DAY, 0);
                        firstDate.set(Calendar.MINUTE, 0);
                        dateStart = firstDate.getTime();

                        if (secondDate == null)
                            secondDate = firstDate;

                        secondDate.set(Calendar.HOUR_OF_DAY, 23);
                        secondDate.set(Calendar.MINUTE, 59);
                        dateEnd = secondDate.getTime();

                        isDataLoaded = false;
                        initializeData();
                    }
                })
                .show(getSupportFragmentManager(), "TAG_SLYCALENDAR");
    }

    private static void writeReportToOutputStream(Context context, String fileName, OutputStream outputStream, List<StudyEvent> studyEvents) {
        if (studyEvents != null) {
            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
            df.setTimeZone(tz);

            try {
                ZipOutputStream out = new ZipOutputStream(outputStream);
                ZipEntry e = new ZipEntry(fileName);
                out.putNextEntry(e);

                // Add metadata
                StringBuffer metaSb = new StringBuffer();
                String[] metaKeys = {
                        "geo-home-lat", "geo-home-long", "geo-home-address", "geo-home-postalcode",
                        "geo-work-lat", "geo-work-long", "geo-work-address", "geo-work-postalcode",
                        "step_time_start", "step_time_stop", PREF_DAYS_OF_WEEK
                };
                List<String> metaKeysArray = Arrays.asList(metaKeys);
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
                Map<String, ?> allEntries = sharedPrefs.getAll();
                for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                    if (metaKeysArray.contains(entry.getKey())) {
                        metaSb.append("meta").append(",")
                                .append(entry.getKey()).append(",")
                                .append("\"").append(entry.getValue().toString()).append("\"")
                                .append("\n");
                    }
                }
                out.write(metaSb.toString().getBytes());

                ArrayList<String> images = new ArrayList<String>();

                for (StudyEvent event : studyEvents) {

                    StringBuffer sb = new StringBuffer();

                    sb.append(event.getId()).append(",");
                    sb.append("\"").append(event.getStudyId()).append("\",");
                    sb.append("\"").append(df.format(new Date(event.getTimestamp()))).append("\",");

                    String formatValue = event.getEventValue();

                    switch (event.getStudyId()) {
                        case StudyEvent.EVENT_TYPE_GEO_LOGGING:
                        case StudyEvent.EVENT_TYPE_APP_USAGE:
                            StringTokenizer st = new StringTokenizer(formatValue, ",");

                            sb.append("\"").append(st.nextToken()).append(",").append(st.nextToken()).append("\"").append(",");
                            while (st.hasMoreTokens()) {
                                sb.append(st.nextToken());

                                if (st.hasMoreTokens())
                                    sb.append(",");
                            }

                            sb.append("\n");
                            break;
                        case StudyEvent.EVENT_TYPE_CAMERA:
                            // export feature of the camera module. The first element of the list is the
                            // image data that is converted to an image to then be zipped and have the
                            // first element of the csv is a pointer to image in the zipped file

                            StringTokenizer cst = new StringTokenizer(formatValue, ",");

                            String imagePointer = cst.nextToken();
                            images.add(imagePointer.replace("Pic:", ""));

                            sb.append("\"").append(imagePointer).append(("\","));
                            while (cst.hasMoreTokens()) {
                                sb.append(cst.nextToken());

                                if (cst.hasMoreTokens())
                                    sb.append(",");
                            }
                            sb.append("\n");
                            break;
                        default:
                            sb.append("\"").append(formatValue).append("\"\n");
                            break;
                    }

                    out.write(sb.toString().getBytes());
                }

                out.closeEntry();

                // the data of the images to be converted to png are stored in a list. This checks if
                // there are images to be converted and placed in a zip file. Skip this if otherwise
                if (!images.isEmpty()) {
                    Set<String> storedImages = new HashSet<>();
                    for (String pic : images) {
                        Uri picUri = Uri.parse(pic);
                        ContentResolver contentResolver = context.getContentResolver();
                        InputStream fi = contentResolver.openInputStream(picUri);
                        try (BufferedInputStream origin = new BufferedInputStream(fi)) {
                            byte data[] = new byte[8192];

                            // Add _i to the file if another one already exists
                            Cursor returnCursor =
                                    contentResolver.query(picUri, null, null, null, null);
                            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            returnCursor.moveToFirst();
                            String filename = returnCursor.getString(nameIndex);
                            returnCursor.close();

                            String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(picUri));
                            String base = filename.replace("." + extension, "");
                            String finalName = base;
                            for (int i = 1; !storedImages.add(finalName); i++)
                                finalName = base + "_" + i;
                            if (!extension.isEmpty()) {
                                finalName += "." + extension;
                            }

                            ZipEntry entry = new ZipEntry(finalName);
                            out.putNextEntry(entry);
                            int count;
                            while ((count = origin.read(data, 0, 8192)) != -1) {
                                out.write(data, 0, count);
                            }
                            out.closeEntry();
                        }
                    }
                }

                out.close();
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        }
    }

    private void selectDatesForFile() {
        Intent intent = new Intent(this,SurveyActivity.class);
        intent.putExtra("survey","data_date");
        mSelectDataDates.launch(intent);
    }

    ActivityResultLauncher<Intent> mSelectDataDates = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        if (intent != null) {
                            DateFormat df = new SimpleDateFormat("M:d:yyyy", Locale.US);
                            try {
                                String startDate = intent.getStringExtra("start_date");
                                String endDate = intent.getStringExtra("end_date");
                                dateStart = df.parse(startDate);
                                dateEnd = df.parse(endDate);
                                dateEnd.setHours(23);
                                dateEnd.setMinutes(59);
                            } catch (ParseException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        isDataLoaded = false;
                        initializeData();
                    }
                }
            }
    );

    private void selectExternalStorageFolder() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_TITLE, REPORT_FILE_NAME + ".zip");
        mSelectExternalStorageFinder.launch(intent);
    }

    ActivityResultLauncher<Intent> mSelectExternalStorageFinder = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        if (intent != null) {
                            Uri contentUri = intent.getData();
                            try {
                                writeReportToOutputStream(getApplicationContext(), REPORT_FILE_NAME,
                                        getContentResolver().openOutputStream(contentUri), mStudyEvents);
                                showSnackbar(getString(R.string.status_file_downloaded));
                            } catch (FileNotFoundException e) {
                                throw new RuntimeException(e);
                            }

                        }
                    }
                }
            }
    );

    private void showSnackbar(String text) {
        Snackbar sb = Snackbar.make(
                this.findViewById(R.id.study_results_activity), text, Snackbar.LENGTH_LONG);
        View v = sb.getView();
        TextView tv = (TextView) v.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        sb.show();
    }

    private static File getReport(Context context, String fileName, List<StudyEvent> studyEvents) {
        File fileReportDir = new File(context.getFilesDir(), "reports");
        File fileReport = new File(fileReportDir, fileName + ".zip");
        fileReport.getParentFile().mkdirs();
        try {
            OutputStream outputStream = new FileOutputStream(fileReport);
            writeReportToOutputStream(context, fileName, outputStream, studyEvents);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return fileReport;
    }

    private void getAndShareReport()
    {
        File fileReport = getReport(getApplicationContext(), REPORT_FILE_NAME, mStudyEvents);
        Uri contentUri = FileProvider.getUriForFile(StudyResultsActivity.this, TrackingService.FILE_AUTHORITY, fileReport);

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sharingIntent.setType("application/zip");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, contentUri);

        // validate that the device can open your File!
        PackageManager pm = getPackageManager();
        if (sharingIntent.resolveActivity(pm) != null) {
            Intent chooser = Intent.createChooser(sharingIntent, getString(R.string.share_results_prompt));
            List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                this.grantUriPermission(packageName, contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivity(chooser);
        }
    }

    public static void getAndSendReport(Context context, String fileName, List<StudyEvent> studyEvents) {

        Amplify.Auth.fetchAuthSession(
                authResult -> {
                    AWSCognitoAuthSession cognitoAuthSession = (AWSCognitoAuthSession) authResult;
                    switch(cognitoAuthSession.getIdentityIdResult().getType()) {
                        case SUCCESS:
                            StorageUploadFileOptions options = StorageUploadFileOptions.builder()
                                    .targetIdentityId(cognitoAuthSession.getIdentityIdResult().getValue())
                                    .build();
                            File fileReport = getReport(context, fileName, studyEvents);

                            Amplify.Storage.uploadFile(
                                    fileReport.getName(),
                                    fileReport,
                                    options,
                                    storageProgress -> Log.d("Amplify", "Fraction completed: " + storageProgress.getFractionCompleted()),
                                    storageResult -> {
                                        Log.d("Amplify", storageResult.getKey());
                                    },
                                    storageError -> {
                                        Log.e("Amplify", storageError.toString());
                                    });

                            break;
                        case FAILURE:
                            Log.e("Amplify", "IdentityId not present because: " + cognitoAuthSession.getIdentityIdResult().getError().toString());
                    }


                },
                authError -> Log.e("Amplify", authError.toString())
        );



    }

    class StudyResult {
        String name;
        String desc;
        int studyIconId;

        StudyResult(String name, String desc, int studyIconId) {
            this.name = name;
            this.desc = desc;
            this.studyIconId = studyIconId;
        }
    }

    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StudyResultViewHolder>{

        private List<StudyResult> mResults;

        public RVAdapter (List<StudyResult> results)
        {
            mResults = results;
        }

        public class StudyResultViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView studyName;
            TextView studyDesc;
           // ImageView studyIcon;

            StudyResultViewHolder(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.cv);
                studyName = itemView.findViewById(R.id.study_name);
                studyDesc = itemView.findViewById(R.id.study_desc);
              //  studyIcon = itemView.findViewById(R.id.study_icon);
            }

        }

        @Override
        public int getItemCount() {
            return mResults.size();
        }

        @Override
        public StudyResultViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.study_card_result, viewGroup, false);
            final StudyResultViewHolder pvh = new StudyResultViewHolder(v);
            pvh.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            return pvh;
        }

        @Override
        public void onBindViewHolder(StudyResultViewHolder studyResultViewHolder, int i) {
            studyResultViewHolder.studyName.setText(mResults.get(i).name);
            studyResultViewHolder.studyDesc.setText(mResults.get(i).desc);
           // studyResultViewHolder.studyIcon.setImageResource(mResults.get(i).studyIconId);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }

    private void deleteAllData () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.title_delete_all_data));
        builder.setMessage(getString(R.string.prompt_are_you_sure));

        builder.setPositiveButton(getString(R.string.answer_yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                StudyViewModel svm = new StudyViewModel(getApplication());
                svm.deleteStudyEvents(mStudyIdFilter,-1,-1);

                dialog.dismiss();

                refresh();
            }
        });

        builder.setNegativeButton(getString(R.string.answer_no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    private void deleteSelectedData () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.title_delete_selected));
        builder.setMessage(getString(R.string.prompt_are_you_sure));

        builder.setPositiveButton(getString(R.string.answer_yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                StudyViewModel svm = new StudyViewModel(getApplication());
                svm.deleteStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime());



                dialog.dismiss();

                refresh();
            }
        });

        builder.setNegativeButton(getString(R.string.answer_no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    boolean isDataLoaded = false;
    List<StudyEvent> mStudyEvents = null;

    public void refresh ()
    {
        mHandler.postDelayed(new Runnable() {

            public void run ()
            {
                initializeData();
            }
        },2000);
    }

    private void initializeData(){

        DateFormat sdf = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT);
        StringBuffer sb = new StringBuffer();
        sb.append(sdf.format(dateStart));
        sb.append(" - ");
        sb.append(sdf.format(dateEnd));

        tvTimeFilter.setText(sb.toString());

        //first clear old studies
        List<StudyResult> studies = new ArrayList<>();
        RVAdapter adapter = new RVAdapter(studies);
        rv.setAdapter(adapter);

        final Snackbar sbar = Snackbar.make(rv,getString(R.string.status_loading),Snackbar.LENGTH_INDEFINITE);
        TextView tv = sbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));

        sbar.show();

        StudyViewModel svm = new StudyViewModel(getApplication());
        svm.getStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                if (isDataLoaded) {
                    if (sbar.isShown()) {
                        sbar.dismiss();
                    }
                    return;
                }

                isDataLoaded = true;

                mStudyEvents = studyEvents;

                List<StudyResult> studies = new ArrayList<>();

                for (StudyEvent event : mStudyEvents) {
                    StudyResult result = new StudyResult(event.getStudyId() + ": " + event.getEventValue(),new Date(event.getTimestamp())+"", R.drawable.study_sitting);
                    studies.add(result);
                }

                RVAdapter adapter = new RVAdapter(studies);
                rv.setAdapter(adapter);

                sbar.dismiss();

                switch (mNextFileAction) {
                    case SHARE:
                        getAndShareReport();
                        break;
                    case DOWNLOAD:
                        selectExternalStorageFolder();
                        break;
                    case SEND:
                        getAndSendReport(getApplicationContext(), REPORT_FILE_NAME, mStudyEvents);
                        break;
                }
                mNextFileAction = FileAction.NONE;
            }
        });



    }

    private static StudyRepository sRepository;

    private static synchronized StudyRepository getRepository (Context context)
    {
        if (sRepository == null)
            sRepository = new StudyRepository(context);

        return sRepository;
    }

    private long baseTime = -1;

    private void initDates ()
    {


        Calendar cal = Calendar.getInstance();

        baseTime = getIntent().getLongExtra("studyDate",-1L);

        if (baseTime != -1L)
            cal.setTimeInMillis(baseTime);
        else
            baseTime = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);

        dateStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        dateEnd = cal.getTime();


    }


}
